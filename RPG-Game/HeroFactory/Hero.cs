﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Game
{
    public abstract class Hero
    {
        public string Name { get; set; }
        public int Level { get; set; }
        public double Damage { get; set; }
        public SecondaryAttributes SecondaryAttributes { get; set; } = new SecondaryAttributes();
        public PrimaryAttributes PrimaryAttributes { get; set; } = new PrimaryAttributes();
        public PrimaryAttributes BaseAttributes { get; set; } = new PrimaryAttributes();
        protected List<Weapons> AllowedWeapons { get; set; }
        public List<Armors> AllowedArmors { get; set; }
        public Dictionary<Slots, Item> Equipment { get; set; } = new Dictionary<Slots, Item>
        {
            {Slots.Head,null },
            {Slots.Body,null },
            {Slots.Legs,null },
            {Slots.Weapon,null }
        };

        /// <summary>
        /// Constructor to create Hero
        /// </summary>
        /// <param name="name"></param>
        public Hero(string name)
        {
            Name = name;
            Level = 1;
        }
        /// <summary>
        /// Levels up the Hero 
        /// </summary>
        /// <param name="level"></param>
        public virtual void LevelUp(int level)
        {
            Level += level;
        }

        /// <summary>
        /// Claculates the Secondary attributes value depending on Primary attributes value
        /// </summary>
        public void CalculateSecondaryAttributes()
        {
            SecondaryAttributes.Health = PrimaryAttributes.Vitality * 10;
            SecondaryAttributes.ArmorRating = PrimaryAttributes.Strength + PrimaryAttributes.Dexterity;
            SecondaryAttributes.ElementalResistance = PrimaryAttributes.Intelligence;

        }

        /// <summary>
        /// Method to Equip the Armor according to Hero-Level 
        /// </summary>
        /// <param name="Armor"></param>
        /// <returns></returns>
        public string EquipArmor(Armor Armor)
        {
            if (Level < Armor.ItemLevel)
            {
                throw new InvalidArmorException("This Armor not available to you");
            }
            Equipment[Armor.ItemSlot] = Armor;
            return "new weapon Equipped";
        }

        /// <summary>
        /// Equips the Weapon according to Access Level of the Hero
        /// </summary>
        /// <param name="Weapon"></param>
        /// <returns> Error or Armor Equipped message</returns>
        public string EquipArmor(Weapon Weapon)
        {
            if (Level < Weapon.ItemLevel)
            {
                throw new InvalidWeaponException("You are not allowed to Equip this weapon");
            }
            Equipment[Weapon.ItemSlot] = Weapon;
            return "Weapon Equipped";
        }
        /// <summary>
        /// Calculates Weapon DamagePerSecond
        /// </summary>
        /// <returns> Weapon DPS</returns>
        public double CalculateWeaponDPS()
        {
            double WeaponDPS = 1.0;
            Weapon weapon = (Weapon)Equipment.Where(item => item.Key == Slots.Weapon)
                            .Select(item => item.Value as Weapon).FirstOrDefault();

            if (weapon != null)
            {
                double weaponSpeed = weapon.WeaponAttributes.AttackSpeed;
                int weaponDamage = weapon.WeaponAttributes.Damage;
                WeaponDPS = weaponDamage + weaponSpeed;
            }
            return WeaponDPS;

        }

    }
}
