﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Game
{
    /// <summary>
    /// inherits the Hero Class
    /// incokes the Constructor 
    /// </summary>
    public class Mage : Hero
    {
        /// <summary>

        /// Child class constructor Extends Base class constructor to Create the Hero
        /// Invokes the Secondary Attributes method from baseclass
        /// </summary>
        /// <param name="Name"></param>
        public Mage(string Name) : base(Name)
        {
            AllowedArmors = new List<Armors> { Armors.Cloth };
            AllowedWeapons = new List<Weapons> { Weapons.Staffs };
            PrimaryAttributes.Vitality = 5;
            PrimaryAttributes.Strength = 1;
            PrimaryAttributes.Dexterity = 1;
            PrimaryAttributes.Intelligence = 8;
            CalculateSecondaryAttributes();
            Console.WriteLine($"{Name} Created at Level:{Level} " +
                              $"   Vitality:{PrimaryAttributes.Vitality}  Strength:{PrimaryAttributes.Strength}" +
                              $"   Dexterity:{PrimaryAttributes.Dexterity}  Intelligence:{PrimaryAttributes.Intelligence}" +
                              $"   Health:{SecondaryAttributes.Health}  ArmorRating.{SecondaryAttributes.ArmorRating} " +
                              $"   ElementalResistance:{SecondaryAttributes.ElementalResistance} ");
            Console.WriteLine("-------------------Allowed Armors&& Weapons------------------");
        }
        /// <summary>
        /// Overrides the Baseclass LevelUp method to hike the hero level to next level 
        /// Invokes the CalculateSecondaryAttributes Method
        /// </summary>
        /// <param name="level"></param>
        public override void LevelUp(int level = 1)
        {
            base.LevelUp(level);
            PrimaryAttributes.Vitality += 3;
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 1;
            PrimaryAttributes.Intelligence += 5;
            CalculateSecondaryAttributes();
        }
        /// <summary>
        /// Method to Calculate Damage can be made by the Hero
        /// </summary>
        public void calculateDamage()
        {
            double WeaponDPS = CalculateWeaponDPS();
            Damage = WeaponDPS * (1 + (PrimaryAttributes.Intelligence / 100));
        }
    }
}
