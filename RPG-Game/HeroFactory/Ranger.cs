﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Game
{
    public class Ranger : Hero
    {
        /// <summary>
        /// Child class constructor Extends Base class constructor to Create the Hero
        /// Invokes the Secondary Attributes method from baseclass
        /// </summary>
        /// <param name="Name"></param>
        public Ranger(string Name) : base(Name)
        {
            AllowedArmors = new List<Armors> { Armors.Leather, Armors.Mail };
            AllowedWeapons = new List<Weapons> { Weapons.Bows };
            PrimaryAttributes.Vitality = 8;
            PrimaryAttributes.Strength = 1;
            PrimaryAttributes.Dexterity = 7;
            PrimaryAttributes.Intelligence = 1;
            CalculateSecondaryAttributes();
            Console.WriteLine($"{Name} Created at Level:{Level} " +
                             $"   Vitality:{PrimaryAttributes.Vitality}  Strength:{PrimaryAttributes.Strength}" +
                             $"   Dexterity:{PrimaryAttributes.Dexterity}  Intelligence:{PrimaryAttributes.Intelligence}" +
                             $"   Health:{SecondaryAttributes.Health}  ArmorRating.{SecondaryAttributes.ArmorRating} " +
                             $"   ElementalResistance:{SecondaryAttributes.ElementalResistance} ");
            Console.WriteLine("-------------------Allowed Armors&& Weapons------------------");
        }
        /// <summary>
        /// Overrides the Baseclass LevelUp method to hike the hero level to next level 
        /// Invokes the CalculateSecondaryAttributes Method
        /// </summary>
        /// <param name="level"></param>
        public override void LevelUp(int level = 1)
        {
            base.LevelUp(level);
            PrimaryAttributes.Vitality += 2;
            PrimaryAttributes.Strength += 1;
            PrimaryAttributes.Dexterity += 5;
            PrimaryAttributes.Intelligence += 1;
            CalculateSecondaryAttributes();
        }
        /// <summary>
        /// Method to Calculate Damage can be made by the Hero
        /// </summary>
        public void calculateDamage()
        {
            double WeaponDPS = CalculateWeaponDPS();
            Damage = WeaponDPS * (1 + (PrimaryAttributes.Dexterity / 100));
        }

    }
}
