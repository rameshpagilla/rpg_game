﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Game
{
    public class Weapon : Item
    {
        public Weapons WeaponType { get; set; }
        public WeaponAttributes WeaponAttributes { get; set; }

        public Weapon(string ItemName, int ItemLevel, Slots SlotItem, Weapons WeaponType,
                      WeaponAttributes WeaponAttributes) : base(ItemName, ItemLevel, SlotItem)
        {
            this.WeaponType = WeaponType;
            this.WeaponAttributes = WeaponAttributes;
        }

    }
}
