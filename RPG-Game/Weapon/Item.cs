﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace RPG_Game
{
    public class Item
    {
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slots ItemSlot { get; set; }

        public Item(string ItemName, int ItemLevel, Slots ItemSlot)
        {
            this.ItemName = ItemName;
            this.ItemLevel = ItemLevel;
            this.ItemSlot = ItemSlot;
        }
    }
}
