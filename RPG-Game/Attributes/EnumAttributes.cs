﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_Game
{
    public enum Weapons
    {
        Axe,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands

    }

    public enum Armors
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    public enum Slots
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
