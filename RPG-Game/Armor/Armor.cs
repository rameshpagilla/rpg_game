﻿
using System;

namespace RPG_Game
{

    public class Armor : Item
    {
        public Armor ArmorType { get; set; }
        public Slots ArmorSlot { get; set; }
        public PrimaryAttributes ArmorAttributes { get; set; }
        public PrimaryAttributes PrimaryAttributes { get; set; }
        public Armor(string ItemName, int ItemLevel, Slots ItemSlot, Armor ArmorType, PrimaryAttributes ArmorAttributes) :
            base(ItemName, ItemLevel, ItemSlot)
        {
            this.ArmorType = ArmorType;
            this.ArmorAttributes = ArmorAttributes;
        }
    }
}
