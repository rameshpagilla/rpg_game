using System;
using Xunit;
using RPG_Game;

namespace RPG_GAME_TEST
{
    public class CreateHeroTest
    {
        [Fact]
        public void Test_Create_Mage()
        {
            Mage m = new Mage("MAGE");
            Assert.Equal(5, m.PrimaryAttributes.Vitality);
            Assert.Equal(1, m.PrimaryAttributes.Strength);
            Assert.Equal(1, m.PrimaryAttributes.Dexterity);
            Assert.Equal(8, m.PrimaryAttributes.Intelligence);
            Assert.Equal(50, m.SecondaryAttributes.Health);
            Assert.Equal(2, m.SecondaryAttributes.ArmorRating);
            Assert.Equal(8, m.SecondaryAttributes.ElementalResistance);
        }

        [Fact]
        public void Test_Create_Ranger()
        {
            Ranger r = new Ranger("Ranger");
            Assert.Equal(8, r.PrimaryAttributes.Vitality);
            Assert.Equal(1, r.PrimaryAttributes.Strength);
            Assert.Equal(7, r.PrimaryAttributes.Dexterity);
            Assert.Equal(1, r.PrimaryAttributes.Intelligence);
            Assert.Equal(80, r.SecondaryAttributes.Health);
            Assert.Equal(8, r.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, r.SecondaryAttributes.ElementalResistance);

        }

        [Fact]
        public void Test_Create_Rogue()
        {
            Rogue ro = new Rogue("Rogue");
            Assert.Equal(8, ro.PrimaryAttributes.Vitality);
            Assert.Equal(2, ro.PrimaryAttributes.Strength);
            Assert.Equal(6, ro.PrimaryAttributes.Dexterity);
            Assert.Equal(1, ro.PrimaryAttributes.Intelligence);
            Assert.Equal(80, ro.SecondaryAttributes.Health);
            Assert.Equal(8, ro.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, ro.SecondaryAttributes.ElementalResistance);

        }

        [Fact]
        public void Test_Create_Warrior()
        {
            Warrior w = new Warrior("Warrior");
            Assert.Equal(10, w.PrimaryAttributes.Vitality);
            Assert.Equal(5, w.PrimaryAttributes.Strength);
            Assert.Equal(2, w.PrimaryAttributes.Dexterity);
            Assert.Equal(1, w.PrimaryAttributes.Intelligence);
            Assert.Equal(100, w.SecondaryAttributes.Health);
            Assert.Equal(7, w.SecondaryAttributes.ArmorRating);
            Assert.Equal(1, w.SecondaryAttributes.ElementalResistance);


        }
    }
    public class LevelUpHeroTest
    {
        [Fact]
        public void Test_LevelUp_Mage()
        {
            Mage m = new Mage("Mage");
            m.LevelUp();
            Assert.Equal(8, m.PrimaryAttributes.Vitality);
            Assert.Equal(2, m.PrimaryAttributes.Strength);
            Assert.Equal(2, m.PrimaryAttributes.Dexterity);
            Assert.Equal(13, m.PrimaryAttributes.Intelligence);
            Assert.Equal(80, m.SecondaryAttributes.Health);
            Assert.Equal(4, m.SecondaryAttributes.ArmorRating);
            Assert.Equal(13, m.SecondaryAttributes.ElementalResistance);

        }

        [Fact]
        public void Test_LevelUp_Ranger()
        {
            Ranger r = new Ranger("Ranger");
            r.LevelUp();
            Assert.Equal(10, r.PrimaryAttributes.Vitality);
            Assert.Equal(2, r.PrimaryAttributes.Strength);
            Assert.Equal(12, r.PrimaryAttributes.Dexterity);
            Assert.Equal(2, r.PrimaryAttributes.Intelligence);
            Assert.Equal(100, r.SecondaryAttributes.Health);
            Assert.Equal(14, r.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, r.SecondaryAttributes.ElementalResistance);

        }

        [Fact]
        public void Test_LevelUp_Rogue()
        {
            Rogue ro = new Rogue("Rogue");
            ro.LevelUp();
            Assert.Equal(11, ro.PrimaryAttributes.Vitality);
            Assert.Equal(3, ro.PrimaryAttributes.Strength);
            Assert.Equal(10, ro.PrimaryAttributes.Dexterity);
            Assert.Equal(2, ro.PrimaryAttributes.Intelligence);
            Assert.Equal(110, ro.SecondaryAttributes.Health);
            Assert.Equal(13, ro.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, ro.SecondaryAttributes.ElementalResistance);
        }

        [Fact]
        public void Test_LevelUp_Warrior()
        {
            Warrior w = new Warrior("Warrior");
            w.LevelUp();
            Assert.Equal(15, w.PrimaryAttributes.Vitality);
            Assert.Equal(8, w.PrimaryAttributes.Strength);
            Assert.Equal(4, w.PrimaryAttributes.Dexterity);
            Assert.Equal(2, w.PrimaryAttributes.Intelligence);
            Assert.Equal(150, w.SecondaryAttributes.Health);
            Assert.Equal(12, w.SecondaryAttributes.ArmorRating);
            Assert.Equal(2, w.SecondaryAttributes.ElementalResistance);
        }
    }
}
