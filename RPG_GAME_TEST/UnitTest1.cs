using System;
using Xunit;
using RPG_Game;

namespace RPG_GAME_TEST
{
    public class UnitTest1
    {
        [Fact]
        public void Test_Mage()
        {
            Mage m = new Mage("MAGE");
            Assert.Equal(5, m.PrimaryAttributes.Vitality);
            Assert.Equal(1, m.PrimaryAttributes.Strength);
            Assert.Equal(1, m.PrimaryAttributes.Dexterity);
            Assert.Equal(8, m.PrimaryAttributes.Intelligence);

        }

        [Fact]
        public void Test_Ranger()
        {
            Ranger r = new Ranger("Ranger");
            Assert.Equal(8, r.PrimaryAttributes.Vitality);
            Assert.Equal(1, r.PrimaryAttributes.Strength);
            Assert.Equal(7, r.PrimaryAttributes.Dexterity);
            Assert.Equal(1, r.PrimaryAttributes.Intelligence);

        }

        [Fact]
        public void Test_Rogue()
        {
            Rogue ro = new Rogue("Rogue");
            Assert.Equal(8, ro.PrimaryAttributes.Vitality);
            Assert.Equal(2, ro.PrimaryAttributes.Strength);
            Assert.Equal(6, ro.PrimaryAttributes.Dexterity);
            Assert.Equal(1, ro.PrimaryAttributes.Intelligence);

        }

        [Fact]
        public void Test_Warrior()
        {
            Warrior w = new Warrior("Warrior");
            Assert.Equal(10, w.PrimaryAttributes.Vitality);
            Assert.Equal(5, w.PrimaryAttributes.Strength);
            Assert.Equal(2, w.PrimaryAttributes.Dexterity);
            Assert.Equal(1, w.PrimaryAttributes.Intelligence);

        }
    }
}
